﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OWT_Challenge.Data;
using OWT_Challenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OWT_Challenge.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class ContactController : Controller
    {
        private readonly AppDbContext _dbContext;

        public ContactController(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region CRUD Contacts
        /// <summary>
        /// Get all Contacts
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetAllContacts()
        {
            return Json(new { data = await _dbContext.Contact.ToListAsync() });
        }

        /// <summary>
        /// Get a contact by id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetContactById(int id)
        {
            // store in a variable the contact corresponding to the ID
            var contact = await _dbContext.Contact.FindAsync(id);

            // check if a contact was found (if the variable is null)
            if (contact == null)
            {
                return NotFound();
            }

            // return the found contact
            return Json(contact);
        }

        /// <summary>
        /// Create a contact
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///     "firstname": "Paul",
        ///     "lastname": "Berclaz",
        ///     "address": "Route du Pont 43",
        ///     "email": "test.paul@gmail.com",
        ///     "phone": "+4112345678"
        ///     }
        ///     
        /// ! Please follow this example to create a contact     
        /// </remarks>
        [HttpPost]
        public async Task<IActionResult> CreateContact([FromBody] ContactModel contact)
        {
            // Set the id of the contact as 0
            contact.ContactID = 0;

            #region Check of email & phone number
            
            // Check if the email is already taken
            if (_dbContext.Contact.Any(c => c.Email == contact.Email))
            {
                return Json(new { success = false, message = "CREATING ERROR: This email is already taken" });
            }

            // Check if the phone number is already taken
            else if (_dbContext.Contact.Any(c => c.Phone == contact.Phone))
            {
                return Json(new { success = false, message = "CREATING ERROR: This phone number is already taken" });
            }
            #endregion

            // add the contact to the DB
            _dbContext.Add(contact);

            // Save the changes on the DB
            await _dbContext.SaveChangesAsync();

            // return the created contact
            return Json(contact);
        }

        /// <summary>
        /// Update a contact
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///     "firstname": "Paul",
        ///     "lastname": "Berclaz",
        ///     "address": "Route du Pont 43",
        ///     "email": "test.paul@gmail.com",
        ///     "phone": "+4112345678"
        ///     }
        ///     
        /// ! Please follow this example to update a contact
        /// </remarks>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateContact(int id, [FromBody]ContactModel contact)
        {
            #region Check of email & phone number
            // Check if the email is already taken
            if (_dbContext.Contact.Where(c=>c.ContactID!=id).Any(c => c.Email == contact.Email))
            {
                return Json(new { success = false, message = "UPDATING ERROR: This email is already taken" });
            }

            // Check if the phone number is already taken
            else if (_dbContext.Contact.Where(c => c.ContactID != id).Any(c => c.Email == contact.Email))
            {
                return Json(new { success = false, message = "UPDATING ERROR: This email is already taken" });
            }

            #endregion
            try
            {
                // Set the given id to the contact written in the body
                contact.ContactID = id;

                // Update the DB
                _dbContext.Update(contact);

                // Save the changes on the DB
                await _dbContext.SaveChangesAsync();

                // Return the updated contact
                return Json(contact);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // In case of DBUpdateConcurrencyException (no contact exist with this ID), return an error
                return Json(new { success = false, message = "UPDATING ERROR: Contact doesn't exist" });
            }
        }

        /// <summary>
        /// Delete a contact
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContact(int id)
        {
            // store in a variable the contact corresponding to the ID
            var dbcontact = await _dbContext.Contact.FirstOrDefaultAsync(c => c.ContactID == id);

            // check if a contact was found (if the variable is null)
            if (dbcontact == null)
            {
                return Json(new { success = false, message = "DELETING ERROR: Contact doesn't exist" });
            }

            // Create a custom string to show which contact was deleted
            string contactMessage = dbcontact.Firstname + " " + dbcontact.Lastname + "(id:" + dbcontact.ContactID + ")";

            // Remove the contact from DB
            _dbContext.Contact.Remove(dbcontact);

            // Save the changes on the DB
            await _dbContext.SaveChangesAsync();

            // Return a custom json message showing which contact was deleted
            return Json(new { success = true, message = "Following Contact deleted: " + contactMessage });
        }
        #endregion

        #region CRUD Contact skills
        /// <summary>
        /// Get the skills of a contact with his ID
        /// </summary>
        [HttpGet("{id}/Skills")]
        public async Task<IActionResult> GetContactSkills(int id)
        {
            // Get all contactSkills corresponding to this contact(id)
            var contactSkills = await _dbContext.ContactSkills.Where(cs => cs.ContactID == id).ToListAsync();

            // Create a list of objects (csVarList : ContactSkill Var List)
            var csVarList = new List<object>();

            foreach (var contactSkill in contactSkills)
            {
                // Get the skill corresponding to this contactSkill.SkillID
                SkillModel tempSkill = await _dbContext.Skill.FindAsync(contactSkill.SkillID);

                // Create a custom object with the SkillID, the name of the skill and the level of it for this contact
                var varSkill = new
                {
                    id = contactSkill.SkillID,
                    name = tempSkill.Name,
                    level = contactSkill.Level
                };

                // Add the custom object to the csVarList
                csVarList.Add(varSkill);
            }

            // Return the csVarList
            return Json(csVarList);
        }

        /// <summary>
        /// Add skill to contact
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///     "SkillID": 2,
        ///     "Level": "Junior"
        ///     }
        /// </remarks>
        [HttpPost("{id}/Skills")]
        public async Task<IActionResult> AddSkillToContact(int id, [FromBody]ContactSkillModel contactSkill)
        {
            #region checkIfExist
            // Check if there is a contact corresponding to this id
            ContactModel dbContact = await _dbContext.Contact.FindAsync(id);

            if (dbContact == null )
            {
                return Json(new { success = false, message = "ADDING SKILL ERROR: Contact doesn't exist" });
            }

            // Check if there is a skill corresponding to this SkillID (contactSkill.SkillID)
            SkillModel dbSkill = await _dbContext.Skill.FindAsync(contactSkill.SkillID);

            if (dbSkill == null)
            {
                return Json(new { success = false, message = "ADDING SKILL ERROR: Skill doesn't exist" });
            }
            #endregion

            // Get all the contact skills in a variable
            var dbContactSkills =  _dbContext.ContactSkills.Where(cs => cs.ContactID == id);


            // Check if between those skills isn't the skill we want to add
            if (!dbContactSkills.Any(cs => cs.SkillID == contactSkill.SkillID))
            {
                // Add the contact ID
                contactSkill.ContactID = id;

                // Add the contactSkill to DB
                _dbContext.Add(contactSkill);

                // Save the changes on the DB
                await _dbContext.SaveChangesAsync();
            }

            // return the contact skills list
            return await GetContactSkills(id);

        }

        /// <summary>
        /// Update skill to contact
        /// </summary>
        /// 
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///     "SkillID": 2,
        ///     "Level": "Junior"
        ///     }
        /// </remarks>
        [HttpPut("{id}/Skills")]
        public async Task<IActionResult> UpdateContactSkill(int id, [FromBody] ContactSkillModel contactSkill)
        {
            #region checkIfExist
            // Check if there is a contact corresponding to this id
            ContactModel dbContact = await _dbContext.Contact.FindAsync(id);

            if (dbContact == null)
            {
                return Json(new { success = false, message = "ADDING SKILL ERROR: Contact doesn't exist" });
            }

            // Check if there is a skill corresponding to this SkillID (contactSkill.SkillID)
            SkillModel dbSkill = await _dbContext.Skill.FindAsync(contactSkill.SkillID);

            if (dbSkill == null)
            {
                return Json(new { success = false, message = "ADDING SKILL ERROR: Skill doesn't exist" });
            }
            #endregion

            var dbContactSkills = _dbContext.ContactSkills.Where(cs => cs.ContactID == id);
            var dbContactSkill = await dbContactSkills.Where(cs => cs.SkillID == contactSkill.SkillID).FirstOrDefaultAsync();

            // Check if between those skills is the skill we want to update
            if (dbContactSkills.Any(cs => cs.SkillID == contactSkill.SkillID))
            {
                // change the level of the skill
                dbContactSkill.Level = contactSkill.Level;

                // update the contactSkill in the db
                _dbContext.Update(dbContactSkill);

                // Save the changes on the DB
                await _dbContext.SaveChangesAsync();
            }
            

            // return the contact skills list
            return await GetContactSkills(id);
        }

        /// <summary>
        /// Remove a skill from contact
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///          "SkillID": 2
        ///     }
        /// </remarks>
        [HttpDelete("{contactID}/Skills")]
        public async Task<IActionResult> RemoveSkillFromContact(int contactID, ContactSkillModel contactSkill)
        {
            #region checkIfExist
            // Check if there is a contact corresponding to this id
            ContactModel dbContact = await _dbContext.Contact.FindAsync(contactID);

            if (dbContact == null)
            {
                return Json(new { success = false, message = "DELETING ERROR: Contact doesn't exist" });
            }

            // Check if there is a skill corresponding to this SkillID (contactSkill.SkillID)
            SkillModel dbSkill = await _dbContext.Skill.FindAsync(contactSkill.SkillID);

            if (dbSkill == null)
            {
                return Json(new { success = false, message = "DELETING ERROR: Skill doesn't exist" });
            }
            #endregion

            // add contactID to contactSkill
            contactSkill.ContactID = contactID;
            contactSkill.SkillID = contactSkill.SkillID;

            // store in a var the contactSkill corresponding to skill we want to remove from the contact we select with the id
            var dbContactSkill = await _dbContext.ContactSkills.Where(s => s.SkillID == contactSkill.SkillID).Where(c => c.ContactID == contactID).FirstOrDefaultAsync();

            // Remove the contactSkill from DB
            _dbContext.Remove(dbContactSkill);

            // Save the changes on the DB
            await _dbContext.SaveChangesAsync();

            // Return the contact skills
            return await GetContactSkills(contactID);
        }
        #endregion

    }


}