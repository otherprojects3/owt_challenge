﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OWT_Challenge.Models
{
    public class ContactSkillModel
    {
        [Key]
        public int ContactSkillID {get; set; }

        public int ContactID { get; set; }
        public ContactModel contact { get; set; }


        public int SkillID { get; set; }
        public SkillModel skill { get; set; }


        public string Level { get; set; }
    }
}
