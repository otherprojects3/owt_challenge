﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OWT_Challenge.Models
{
    public class SkillModel
    {
        [Key]
        public int SkillID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ContactSkillModel> ContactSkills { get; set; }
    }
}
