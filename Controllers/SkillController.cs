﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OWT_Challenge.Data;
using OWT_Challenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OWT_Challenge.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class SkillController : Controller
    {
        private readonly AppDbContext _dbContext;

        public SkillController(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get all Skills
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetAllSkills()
        {
            // Return all the skills in the DB
            return Json(new { data = await _dbContext.Skill.ToListAsync() });
        }

        /// <summary>
        /// Get a skill by id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSkillById(int id)
        {
            // store in a variable the skill corresponding to the ID
            var skill = await _dbContext.Skill.FindAsync(id);

            // check if a skill was found (if the variable is null)
            if (skill == null)
            {
                return NotFound();
            }

            // return the found skill
            return Json(skill);
        }

        /// <summary>
        /// Create a skill
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///     "Name": "SQL"
        ///     }
        ///     
        /// ! Please follow this example to create a skill
        /// </remarks>
        [HttpPost]
        public async Task<IActionResult> CreateSkill([FromBody] SkillModel skill)
        {
            // Check if this skill already exists in the DB
            if (_dbContext.Skill.Any(s => s.Name == skill.Name))
            {
                return Json(new { success = false, message = "CREATING ERROR: This skill already exists!" });
            }

            // add the skill to the DB
            _dbContext.Skill.Add(skill);

            // Save the changes on the DB
            await _dbContext.SaveChangesAsync();

            // return the created skill
            return Json(skill);
        }

        /// <summary>
        /// Update a skill
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     {
        ///     "Name": "SQL"
        ///     }
        ///     
        /// ! Please follow this example to update a skill
        /// </remarks>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateSkill(int id, [FromBody] SkillModel skill)
        {
            // Check if this skill already exists in the DB
            if (_dbContext.Skill.Where(s => s.SkillID != id).Any(s => s.Name == skill.Name))
            {
                return Json(new { success = false, message = "UPDATING ERROR: This skill already exists!" });
            }

            try
            {
                // Set the given id to the skill written in the body
                skill.SkillID = id;

                // Update the DB
                _dbContext.Update(skill);

                // Save the changes on the DB
                await _dbContext.SaveChangesAsync();

                // Return the updated skill
                return Json(skill);
            }
            catch(DbUpdateConcurrencyException ex)
            {
                return Json(new { success = false, message = "UPDATING ERROR: Skill doesn't exist" });
            }
        }

        /// <summary>
        /// Delete a skill by ID
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSKill(int id)
        {
            // store in a variable the skill corresponding to the ID
            var dbSkill = await _dbContext.Skill.FirstOrDefaultAsync(s => s.SkillID == id);

            // check if a skill was found (if the variable is null)
            if (dbSkill == null)
            {
                return Json(new { success = false, message = "DELETING ERROR: Skill doesn't exist" });
            }

            // Create a custom string to show which skill was deleted
            string skillMessage = dbSkill.Name + "(id:" + dbSkill.SkillID + ")";

            // Remove the skill from DB
            _dbContext.Skill.Remove(dbSkill);

            // Save the changes on the DB
            await _dbContext.SaveChangesAsync();

            // Return a custom json message showing which skill was deleted
            return Json(new { success = true, message = "Following Skill deleted: " + skillMessage });
        }
    }
}
