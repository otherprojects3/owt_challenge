﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OWT_Challenge.Models
{
    public class ContactModel
    {
		[Key]
		public int ContactID { get; set; }
		public string Firstname { get; set; }
		public string Lastname { get; set; }
		public string Address { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }

		public virtual ICollection<ContactSkillModel> ContactSkills { get; set; }

        public static implicit operator Task<object>(ContactModel v)
        {
            throw new NotImplementedException();
        }
    }
}
