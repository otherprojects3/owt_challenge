﻿using Microsoft.EntityFrameworkCore;
using OWT_Challenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OWT_Challenge.Data
{
    public class AppDbContext : DbContext
    {
        public DbSet<ContactModel> Contact { get; set; }
        public DbSet<SkillModel> Skill { get; set; }
        public DbSet<ContactSkillModel> ContactSkills { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) :base(options){}



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ContactSkillModel>()
            //    .HasKey(x => new { x.ContactID, x.SkillID});

            modelBuilder.Entity<ContactSkillModel>()
                .HasOne(bc => bc.contact)
                .WithMany(b => b.ContactSkills)
                .HasForeignKey(Bc => Bc.ContactID);

            modelBuilder.Entity<ContactSkillModel>()
                .HasOne(bc => bc.skill)
                .WithMany(b => b.ContactSkills)
                .HasForeignKey(Bc => Bc.SkillID);
        }
    }
}
